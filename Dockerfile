FROM python:3-onbuild
ENV PYTHONUNBUFFERED 1
RUN mkdir /src
RUN mkdir /config
RUN mkdir /static
ADD requirements.txt /config/
WORKDIR /src
RUN pip install -r /config/requirements.txt
