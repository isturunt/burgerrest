from rest_framework import serializers

from menu.models import Dish
from restaurants.models import Restaurant, Order, OrderOperator, OrderDish

__author__ = 'isturunt'


class RestaurantSerializer(serializers.ModelSerializer):

    city = serializers.CharField(source='get_city_display', read_only=True)

    class Meta:
        model = Restaurant
        fields = ('id', 'name', 'city')


class OrderOperatorSerializer(serializers.ModelSerializer):

    class Meta:
        model = OrderOperator
        fields = ('id', 'name')


class OrderDishSerializer(serializers.ModelSerializer):

    name = serializers.CharField(source='dish.name')
    description = serializers.CharField(source='dish.description')
    item_cost = serializers.IntegerField()
    total_cost = serializers.IntegerField()

    class Meta:
        model = OrderDish
        fields = ('dish_id', 'name', 'description', 'amount', 'item_cost', 'total_cost')


class OrderSerializer(serializers.ModelSerializer):

    restaurant = RestaurantSerializer()
    dishes = OrderDishSerializer(many=True, source='ordered_dishes')
    operator = OrderOperatorSerializer()
    status = serializers.CharField(source='get_status_display', read_only=True)

    class Meta:
        model = Order
        fields = ('id', 'created_at', 'dishes', 'restaurant', 'operator', 'status', 'total')


class OrderDishPostIDSerializer(serializers.ModelSerializer):

    dish = serializers.PrimaryKeyRelatedField(queryset=Dish.objects.all())

    class Meta:
        model = OrderDish
        fields = ('dish', 'amount')

    def create(self, validated_data):
        return OrderDish(dish_id=validated_data['dish'], amount=validated_data['amount'])


class OrderPostIDSerializer(serializers.ModelSerializer):

    restaurant = serializers.PrimaryKeyRelatedField(queryset=Restaurant.objects.all())
    operator = serializers.PrimaryKeyRelatedField(queryset=OrderOperator.objects.all())
    ordered_dishes = OrderDishPostIDSerializer(many=True)

    class Meta:
        model = Order
        fields = ('ordered_dishes', 'restaurant', 'operator')

    def create(self, validated_data):
        dish_dict_list = validated_data.pop('ordered_dishes', list())
        order = Order.objects.create(**validated_data)

        for dish_dict in dish_dict_list:
            order_dish = OrderDish(
                dish=dish_dict['dish'],
                amount=dish_dict['amount'],
                order=order
            )
            order_dish.save()
        return order
