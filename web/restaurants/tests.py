from django.core.exceptions import ValidationError
from django.db import IntegrityError
from django.test import TestCase

from menu.models import Dish, Category
from restaurants.models import Order, Restaurant, OrderOperator, OrderDish


class OrderTestCase(TestCase):

    def setUp(self):
        cat = Category.objects.create(name='Test Category')
        d1 = Dish.objects.create(name="dish 1", category=cat, cost=1)
        d2 = Dish.objects.create(name="dish 2", category=cat, cost=1)
        d3 = Dish.objects.create(name="dish 3", category=cat, cost=1)

        rest = Restaurant.objects.create(name='Restaurant')
        op = OrderOperator.objects.create(name="Antony")
        order = Order.objects.create(
            operator=op,
            restaurant=rest
        )

        self.dishes = (d1, d2, d3)
        self.amounts = (2, 3, 4)
        self.order = order

        for dish, amount in zip(self.dishes, self.amounts):
            OrderDish.objects.create(dish=dish, order=order, amount=amount)

    def test_order_total_sum_should_equal_weighted_sum_of_dish_costs(self):
        self.assertEqual(
            self.order.total,
            sum(map(lambda item: item[0].cost * item[1], zip(self.dishes, self.amounts))),
            "Order total sum should equal the sum of total costs (cost multiplied by amount) of its dishes"
        )

    def test_order_should_preserve_dish_cost(self):

        test_dish = self.dishes[0]
        original_cost = test_dish.cost
        test_dish.cost += 100
        test_dish.save()

        order_dish = OrderDish.objects.get(
            dish=test_dish,
            order=self.order
        )
        self.assertEqual(
            order_dish.item_cost, original_cost,
            "Altering a dish cost should NOT change the cost of dish already ordered. "
            "Orders should preserve dish costs."
        )

        test_dish.cost -= 100
        test_dish.save()

    def test_negative_amounts_should_not_be_accepted(self):
        with self.assertRaises(IntegrityError):
            OrderDish.objects.create(dish=self.dishes[0], order=self.order, amount=-1)
            OrderDish.objects.create(dish=self.dishes[0], order=self.order, amount=0)
