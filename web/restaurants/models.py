from django.core.validators import MinValueValidator
from django.db import models

from menu.models import Dish


class Restaurant(models.Model):

    CITY_CHOICES = (
        ('MSC', 'Москва'),
        ('SPB', 'Санкт-Петербург')
    )

    name = models.CharField(max_length=100)
    city = models.CharField(max_length=3, choices=CITY_CHOICES)

    class Meta:
        verbose_name = 'Ресторан'
        verbose_name_plural = 'Рестораны'

    def __repr__(self):
        return "{name} ({city})".format(
            name=self.name,
            city=self.get_city_display()
        )

    def __str__(self):
        return repr(self)


class OrderOperator(models.Model):

    class Meta:
        verbose_name = 'Оператор заказов'
        verbose_name_plural = 'Операторы заказов'

    name = models.CharField(max_length=80)

    def __repr__(self):
        return self.name

    def __str__(self):
        return repr(self)


class OrderDish(models.Model):
    order = models.ForeignKey('Order', on_delete=models.CASCADE, related_name='ordered_dishes')
    dish = models.ForeignKey(Dish, on_delete=models.CASCADE)
    amount = models.PositiveSmallIntegerField(default=1)
    item_cost = models.FloatField()

    class Meta:
        db_table = 'restaurants_order_dishes'

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.item_cost = self.dish.cost
        super(OrderDish, self).save(force_insert=force_insert, force_update=force_update,
                                    using=using, update_fields=update_fields)

    @property
    def total_cost(self):
        return self.item_cost * self.amount

    def __repr__(self):
        return "{dish} ({cost}) x{amount}".format(
            dish=repr(self.dish.name),
            cost=self.item_cost,
            amount=self.amount
        )

    def __str__(self):
        return repr(self)


class Order(models.Model):

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

    STATUS_CHOICES = (
        ('A', 'принят'),
        ('P', 'оплачен'),
        ('C', 'отменен'),
    )

    dishes = models.ManyToManyField(to=Dish, through=OrderDish)
    created_at = models.DateTimeField(auto_now_add=True)
    restaurant = models.ForeignKey(to=Restaurant, blank=False, null=False, on_delete=models.CASCADE)
    operator = models.ForeignKey(to=OrderOperator, null=True, on_delete=models.SET_NULL)
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, default='A', blank=False)

    @property
    def total(self):
        return sum([item.total_cost for item in self.ordered_dishes.all()])

    @property
    def all_ordered_dishes(self):
        return list(self.ordered_dishes.all())

    def __repr__(self):
        return "Заказ {}".format(self.id)

    def __str__(self):
        return repr(self)
