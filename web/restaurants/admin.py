from django.contrib import admin

from restaurants.models import Restaurant, Order, OrderOperator


class OrderAdmin(admin.ModelAdmin):
    model = Order
    list_filter = ('restaurant__city', 'created_at', 'restaurant', 'operator')
    readonly_fields = ('created_at', 'all_ordered_dishes', 'restaurant', 'operator', 'total')
    fields = readonly_fields + ('status', )


admin.site.register(Restaurant)
admin.site.register(OrderOperator)
admin.site.register(Order, OrderAdmin)
