from django.apps import AppConfig


class RestaurantsConfig(AppConfig):
    name = 'restaurants'
    verbose_name = 'Рестораны и заказы'
