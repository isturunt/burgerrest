#!/bin/bash

echo Running migrations
python manage.py makemigrations; python manage.py migrate
echo Running test
python manage.py test
if [ $? -ne 0 ]
then
    echo Run canceled due to test failure
    exit 1
fi
echo Collecting static
pyhon manage.py collectstatic --noinput
echo Running Gunicorn
exec gunicorn BurgerRest.wsgi:application --reload --bind 0.0.0.0:8000 --workers 3