from django.db import IntegrityError, transaction
from django.test import TestCase

from menu.models import Dish, Category


class DishTestCase(TestCase):

    def test_should_not_save_dish_with_no_category(self):
        with self.assertRaises(IntegrityError):
            d = Dish(name="Rogue dish", description="This dish ain't got no home", cost=1)
            d.save()

    def test_dish_cannot_have_negative_cost(self):
        with self.assertRaises(IntegrityError):
            Dish.objects.create(
                name="Uneatable",
                description="This one is so ugly we would pay you to eat it",
                cost=-100
            )

    def test_dishes_should_have_unique_names(self):
        with self.assertRaises(IntegrityError):
            Dish.objects.create(
                name='test non-unique name',
                cost=100
            )
            Dish.objects.create(
                name='test non-unique name',
                cost=200
            )


class CategoryTestCase(TestCase):

    def test_category_names_should_be_unique_within_one_parent(self):
        with self.assertRaises(IntegrityError):
            parent_cat = Category.objects.create(name="Test Unique Parent Cat")
            Category.objects.create(name="Test Unique Child", parent=parent_cat)
            Category.objects.create(name="Test Unique Child", parent=parent_cat)
