from django.core.validators import MinValueValidator
from django.db import models


class Dish(models.Model):
    name = models.CharField(max_length=200, blank=False, unique=True)
    description = models.TextField(blank=True)
    category = models.ForeignKey('Category', on_delete=models.CASCADE, blank=False, null=False, related_name='dishes')
    cost = models.FloatField(blank=False, validators=[MinValueValidator(limit_value=0)])

    class Meta:
        verbose_name = 'Блюдо'
        verbose_name_plural = 'Блюда'

    def __repr__(self):
        return "{name} ({cost})".format(
            name=self.name,
            cost=self.cost
        )

    def __str__(self):
        return repr(self)


class Category(models.Model):
    name = models.CharField(max_length=200, blank=False)
    parent = models.ForeignKey(to='self', on_delete=models.SET_NULL, blank=True, null=True)

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'
        unique_together = (('name', 'parent'),)

    @property
    def children(self):
        return self.__class__.objects.filter(parent_id=self.id)

    def __repr__(self):
        return self.name

    def __str__(self):
        return repr(self)
