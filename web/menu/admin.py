from django.contrib import admin

from menu.models import Category, Dish


class CategoryAdmin(admin.ModelAdmin):

    fields = ('name', 'parent')
    empty_value_display = "None"


class DishAdmin(admin.ModelAdmin):

    fields = ('name', 'description', 'cost', 'category')
    list_filter = ('category__name',)


admin.site.register(Category, CategoryAdmin)
admin.site.register(Dish, DishAdmin)
