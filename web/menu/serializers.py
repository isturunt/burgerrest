from rest_framework import serializers
from rest_framework_recursive.fields import RecursiveField

from menu.models import Dish, Category

__author__ = 'isturunt'


class DishSerializer(serializers.ModelSerializer):

    class Meta:
        model = Dish
        fields = ('id', 'name', 'description', 'cost')


class CategorySerializer(serializers.ModelSerializer):

    dishes = DishSerializer(many=True)
    children = serializers.ListField(child=RecursiveField())

    class Meta:
        model = Category
        fields = ('name', 'dishes', 'children')
