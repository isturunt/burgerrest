from rest_framework import viewsets

from menu.models import Category
from menu.serializers import CategorySerializer

__author__ = 'isturunt'


class MenuViewSet(viewsets.ModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.filter(parent=None)
