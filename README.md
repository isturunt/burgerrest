# Burger Restaurant Test Project

## Configuration

In project root, create the _.env_ file. Check out _.env.example_ file and use it as a template for your _.env_.


## Build & Run

Make sure that you have successfully created _.env_ and specified __all__ missing values. Otherwise it won't work.

To build and run execute the following command:

    docker-compose build && docker-compose up

### Users

Don't forget to [create a superuser](https://docs.djangoproject.com/en/2.1/intro/tutorial02/#creating-an-admin-user). Run the following command:

    docker exec -ti burgers_web python manage.py createsuperuser


## Auth

User privileges are specified via django [native permissions](https://docs.djangoproject.com/en/2.1/topics/auth/default/#default-permissions)

### Manager

Login page:

`/admin/`

(E.g., `http://localhost:8000/admin/`)

Managers are users with permissions for CRUD operations on categories and dishes. They can also view orders and change order status.


### Cashier users

Cashier users are not privileged to login into the site Admin system. However, they are able to use the cashier API. Here a JWT ([JSON Web Token](https://medium.com/vandium-software/5-easy-steps-to-understanding-json-web-tokens-jwt-1164c0adfcec)) authentication is used. In order to perform API calls a user has to obtain a token and sign all his calls with it.


#### Obtain token

**POST** `/auth/obtain_token/`

Body (application/json):

    {
        "username": "<username>",
        "password": "<password>"
    }

For the correct username and password the response would be the following:

    {
        "token": "<your_token>"
    }

Now you can make API calls signed with `<your_token>`: set the `Authorization` header to `JWT <your_token>` and go on.


#### Refresh token

**POST** `/auth/refresh_token/`

Body (application/json):

    {
        "token": "<your_token>"
    }

Here `<your_token>` is your _current_ token. Your response shoul look as follows:

    {
        "token": "<your_new_token>"
    }

Now you should use `<your_new_token>` in your requests.

In case the token has exceeded its lifespan an error occurs (`400 Bad Request`):

    {
        "non_field_errors": "Signature has expired."
    }

It is up to a client application to decide how often to refresh a token and to prompt a user to relogin after token lifespan is exceeded.

## API

###  POST `/auth/obtain_token/`

Obtain token (JWT)

#### Body

    {
        "username": "<username>",
        "password": "<password>"
    }

#### Response

    {
        "token": "<your_token>"
    }

###  POST `/auth/refresh_token/`

Refresh token (JWT)

#### Body

    {
        "token": "<your_token>"
    }

#### Response

    {
        "token": "<your_new_token>"
    }


### GET `/menu.json`

Get menu (a hierarchy of categories with dishes)

#### Headers

`Authorization`: `JWT <your_token>`

#### Example Response

    [
        {
            "name": "Напитки",
            "dishes": [
                {
                    "id": 6,
                    "name": "Brewdog: Jack Hammer",
                    "description": "",
                    "cost": 250
                },
                {
                    "id": 5,
                    "name": "Brewdog: Elvis Juice",
                    "description": "",
                    "cost": 260
                },
                {
                    "id": 4,
                    "name": "Brewdog: Punk IPA",
                    "description": "IPA от Brewdog",
                    "cost": 250
                }
            ],
            "children": []
        },
        {
            "name": "Бургеры",
            "dishes": [],
            "children": [
                {
                    "name": "Острые бургеры",
                    "dishes": [
                        {
                            "id": 3,
                            "name": "Diablo",
                            "description": "Острый бургер из говяжьей котлеты, с соусом из перцев хабанеро, кусочками перца халапеньо и сыром. Прочие ингредиенты: салат айсберг, маринованный огурец, красный лук, помидоры.",
                            "cost": 350
                        }
                    ],
                    "children": []
                },
                {
                    "name": "Классические бургеры",
                    "dishes": [
                        {
                            "id": 2,
                            "name": "Чизбургер",
                            "description": "Классический чизбургер. Котлета из говядины, сыр чеддар, салат айсберг, булочка с кунжутом",
                            "cost": 270
                        },
                        {
                            "id": 1,
                            "name": "Гамбургер",
                            "description": "Классический гамбургер. Котлета из говядины, салат айсберг, булочка с кунжутом",
                            "cost": 250
                        }
                    ],
                    "children": []
                }
            ]
        }
    ]


### GET `/orders.json`

Get orders list

#### Headers

`Authorization`: `JWT <your_token>`

#### Example Response

    [
        {
            "id": 20,
            "created_at": "2018-11-19T06:21:32.739305Z",
            "dishes": [
                {
                    "dish_id": 1,
                    "name": "Гамбургер",
                    "description": "Классический гамбургер. Котлета из говядины, салат айсберг, булочка с кунжутом",
                    "amount": 1,
                    "item_cost": 250,
                    "total_cost": 250
                },
                {
                    "dish_id": 5,
                    "name": "Brewdog: Elvis Juice",
                    "description": "",
                    "amount": 2,
                    "item_cost": 250,
                    "total_cost": 500
                }
            ],
            "restaurant": {
                "id": 1,
                "name": "Burger 1",
                "city": "Москва"
            },
            "operator": {
                "id": 1,
                "name": "Jack Brown"
            },
            "status": "принят",
            "total": 750
        }
    ]


### POST `/orders.json`

Create new order

#### Headers

`Authorization`: `JWT <your_token>`


#### Body (application/json)

    {
        "ordered_dishes": [
            {
                "dish": <dish_id_1>,
                "amount": <dish_id_1_amount>
            },
            {
                "dish": <dish_id_2>,
                "amount": <dish_id_2_amount>
            },
            ...
        ],
        "operator": <operator_id>,
        "restaurant": <restaurant_id>
    }

#### Response

Either the created order or error info